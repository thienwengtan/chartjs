import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import data2 from '../../assets/input2.json';
import { Chart, registerables } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit {
  private ticket = data2;

  @ViewChild('barChart') barChart;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('lineCanvas') lineCanvas;

  bars: any;
  colorArray: any;
  doughnutChart: any;
  lineChart: any;

  color: string[] = [];
  colorH: string[] = [];
  status: string[] = [];
  ticketname: string[] = [];
  ticketid: string[] = [];
  totalticket: string[] = [];

  constructor() {
    Chart.register(...registerables, ChartDataLabels);
    console.log(this.ticket);
  }

  ngAfterViewInit() {
    this.ticket.forEach((x) => {
      this.ticketname.push(x.ticketName);
      this.status.push(x.status);
      this.totalticket.push(x.totalticket);
    });
    this.createBarChart();
    this.doughnutChartMethod();
  }

  ngOnInit() {}

  createBarChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'bar',
      data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'November',
          'December',
        ],
        datasets: [
          {
            label: 'Total Ticket Passover',
            data: this.totalticket,
            backgroundColor: this.getColors(this.totalticket.length),
            borderWidth: 1,
          },
        ],
      },
      options: {
        plugins: {
          datalabels: { color: '#000000' },
          legend: {
            display: false,
          },
        },
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }

  doughnutChartMethod() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: this.ticketname,
        datasets: [
          {
            label: 'Performance reporting',
            data: this.totalticket,
            backgroundColor: this.getColors(this.totalticket.length),
            borderWidth: 1,
          },
        ],
      },
      options: {
        plugins: {
          datalabels: { color: '#000000' },
          legend: {
            position: 'right',
          },
        },
      },
    });
  }

  getColors(length) {
    let colors = [];

    for (let i = 0; i < length; i++) {
      var r = Math.floor(Math.random() * 255);
      var g = Math.floor(Math.random() * 255);
      var b = Math.floor(Math.random() * 255);
      colors.push('rgba(' + r + ',' + g + ',' + b + ', 0.5)');
    }

    return colors;
  }
}
